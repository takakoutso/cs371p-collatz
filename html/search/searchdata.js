var indexSectionsWithContent =
{
  0: "cgilmrt",
  1: "crt",
  2: "cgmt",
  3: "l",
  4: "i",
  5: "c"
};

var indexSectionNames =
{
  0: "all",
  1: "files",
  2: "functions",
  3: "variables",
  4: "defines",
  5: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Files",
  2: "Functions",
  3: "Variables",
  4: "Macros",
  5: "Pages"
};

