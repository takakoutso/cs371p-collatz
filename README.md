# CS371p: Object-Oriented Programming Collatz Repo

* Name: Vasilis (Taka) Koutsomitopoulos

* EID: vtk97

* GitLab ID: takakoutso

* HackerRank ID: taka_koutso

* Git SHA: fec19162a1e641f90de2249f3da14592e11e7b30

* GitLab Pipelines: https://gitlab.com/takakoutso/cs371p-collatz/-/pipelines

* Estimated completion time: 5.0

* Actual completion time: 4.0

* Comments: I couldn't maintain high coverage, even though my tests test every single branch of my code. I think it is caused in part due to my lazy caching, since many branches wouldn't be taken often.
