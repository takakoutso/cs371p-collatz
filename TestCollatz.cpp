// ---------------
// TestCollatz.c++
// ---------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include <sstream>  // istringtstream, ostringstream
#include <tuple>    // make_tuple, tuple
#include <utility>  // make_pair, pair

#include "gtest/gtest.h"

#include "Collatz.hpp"

using namespace std;


// ----
// get_cycle_length
// ----

TEST(CollatzFixture, get_cycle_length0) {
    ASSERT_EQ(get_cycle_length(720722), 411);
}

TEST(CollatzFixture, get_cycle_length1) {
    ASSERT_EQ(get_cycle_length(1), 1);
}

TEST(CollatzFixture, get_cycle_length2) {
    ASSERT_EQ(get_cycle_length(999999), 259);
}

TEST(CollatzFixture, get_cycle_length3) {
    ASSERT_EQ(get_cycle_length(626331), 509);
}

TEST(CollatzFixture, get_cycle_length4) {
    ASSERT_EQ(get_cycle_length(837799), 525);
}


// ----
// read
// ----

TEST(CollatzFixture, read) {
    ASSERT_EQ(collatz_read("1 10\n"), make_pair(1, 10));
}

// ----
// eval
// ----

TEST(CollatzFixture, eval0) {
    ASSERT_EQ(collatz_eval(make_pair(1, 10)), make_tuple(1, 10, 20));
}

TEST(CollatzFixture, eval1) {
    ASSERT_EQ(collatz_eval(make_pair(100, 200)), make_tuple(100, 200, 125));
}

TEST(CollatzFixture, eval2) {
    ASSERT_EQ(collatz_eval(make_pair(201, 210)), make_tuple(201, 210, 89));
}

TEST(CollatzFixture, eval3) {
    ASSERT_EQ(collatz_eval(make_pair(900, 1000)), make_tuple(900, 1000, 174));
}

TEST(CollatzFixture, eval4) {
    ASSERT_EQ(collatz_eval(make_pair(749566, 710579)), make_tuple(749566, 710579, 424));
}

TEST(CollatzFixture, eval5) {
    ASSERT_EQ(collatz_eval(make_pair(302854, 385429)), make_tuple(302854, 385429, 441));
}

TEST(CollatzFixture, eval6) {
    ASSERT_EQ(collatz_eval(make_pair(828550, 699263)), make_tuple(828550, 699263, 504));
}

TEST(CollatzFixture, eval7) {
    ASSERT_EQ(collatz_eval(make_pair(1, 1)), make_tuple(1, 1, 1));
}

TEST(CollatzFixture, eval8) {
    ASSERT_EQ(collatz_eval(make_pair(1, 828550)), make_tuple(1, 828550, 509));
}

TEST(CollatzFixture, eval9) {
    ASSERT_EQ(collatz_eval(make_pair(1, 999999)), make_tuple(1, 999999, 525));
}

// -----
// print
// -----

TEST(CollatzFixture, print) {
    ostringstream sout;
    collatz_print(sout, make_tuple(1, 10, 20));
    ASSERT_EQ(sout.str(), "1 10 20\n");
}

// -----
// solve
// -----

TEST(CollatzFixture, solve0) {
    istringstream sin("1 10\n100 200\n201 210\n900 1000\n");
    ostringstream sout;
    collatz_solve(sin, sout);
    ASSERT_EQ(sout.str(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n");
}

TEST(CollatzFixture, solve1) {
    istringstream sin("1 828550\n828550 3\n");
    ostringstream sout;
    collatz_solve(sin, sout);
    ASSERT_EQ(sout.str(), "1 828550 509\n828550 3 509\n");
}

TEST(CollatzFixture, solve2) {
    istringstream sin("1 999999\n500000 999999\n");
    ostringstream sout;
    collatz_solve(sin, sout);
    ASSERT_EQ(sout.str(), "1 999999 525\n500000 999999 525\n");
}
