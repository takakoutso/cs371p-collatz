// -----------
// Collatz.c++
// -----------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <tuple>    // make_tuple, tie, tuple
#include <utility>  // make_pair, pair

#include "Collatz.hpp"

using namespace std;

int lazy_cache[INPUT_RANGE] = {0};    /*!< lazy cache to keep track of values, initialize all to 0 */

// ------------
// get_cycle_length
// ------------

int get_cycle_length(int val) {
    assert (val > 0 && val < INPUT_RANGE);    // check precondition, val > 0
    if (lazy_cache[val] != 0) {
        return lazy_cache[val];        // Optimization 3: if cached, return cached value
    }
    int cycle = 1;
    long long int value = val;
    while (value != 1) {
        if (value % 2 == 0) {
            value >>= 1;               // according to Collatz, evens divided by 2
            cycle++;
        } else {
            value += (value >> 1) + 1; // Optimization 1: performs 3n + 1 and n/2 simultaneously
            cycle += 2;
        }
    }
    lazy_cache[val] = cycle;           // Cache the value for later use
    return cycle;
}


// ------------
// collatz_read
// ------------

pair<int, int> collatz_read (const string& s) {
    istringstream sin(s);
    int i, j;
    sin >> i >> j;
    return make_pair(i, j);
}

// ------------
// collatz_eval
// ------------

tuple<int, int, int> collatz_eval (const pair<int, int>& p) {
    int i, j;
    tie(i, j) = p;
    assert(i > 0 && j > 0 && i < INPUT_RANGE && j < INPUT_RANGE); // check preconditions
    int start = i;
    int end = j;
    if (end < start) {                    // Assume that i < j, account for converse
        start = j;
        end = i;
    }

    int m = end / 2 + 1;                  // Optimization 2, cut range
    if (start < m) {
        start = m;
    }

    int max_cycle = 0;
    for (int i = start; i <= end; i++) {  // iterate from lower number to higher inclusive
        int cycle = get_cycle_length(i);
        if (cycle > max_cycle) {
            max_cycle = cycle;
        }
    }
    return make_tuple(i, j, max_cycle);
}

// -------------
// collatz_print
// -------------

void collatz_print (ostream& sout, const tuple<int, int, int>& t) {
    int i, j, v;
    tie(i, j, v) = t;
    sout << i << " " << j << " " << v << endl;
}

// -------------
// collatz_solve
// -------------

void collatz_solve (istream& sin, ostream& sout) {
    string s;
    while (getline(sin, s))
        collatz_print(sout, collatz_eval(collatz_read(s)));
}
